import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CrearPersonaDTO } from './estudiantes/dto/crear-persona-dto';
import { GradoEntity } from './estudiantes/entities/grados.entity';
import { PersonaEntity } from './estudiantes/entities/persona.entity';
import { CrearGradoDTO } from "./estudiantes/dto/crear-grado-dto";

@Injectable()
export class AppService {

  constructor(
    @InjectRepository(PersonaEntity) private personaRepository: Repository<PersonaEntity>,
    @InjectRepository(GradoEntity) private gradoRepository: Repository<GradoEntity>
  ) { }

  loadData(): string {
    this.seedPersona();
    this.seedGrados();

    return 'Se crearon los datos para las tablas persona y grados.';
  }

  seedPersona(): void {
    const persona: CrearPersonaDTO = {
      nom_persona: 'Joshua André',
      ape_pate_pers: 'Villena',
      ape_mate_pers: 'Diaz',
      id_grado: 5,
      fecha_naci: new Date(),
      foto_ruta: ''
    }
    this.personaRepository.save(persona);
  }

  seedGrados(): void {
    const grados: CrearGradoDTO[] = [
      {
        id_grado: 1,
        desc_grado: '1 AÑO',
        nivel: 'Inicial'
      },
      {
        id_grado: 2,
        desc_grado: '2 AÑO',
        nivel: 'Inicial'
      },
      {
        id_grado: 3,
        desc_grado: '3 AÑO',
        nivel: 'Inicial'
      },
      {
        id_grado: 4,
        desc_grado: '4 AÑO',
        nivel: 'Inicial'
      },
      {
        id_grado: 5,
        desc_grado: '5 AÑO',
        nivel: 'Inicial'
      },
      {
        id_grado: 6,
        desc_grado: 'Primero',
        nivel: 'Primaria'
      },
      {
        id_grado: 7,
        desc_grado: 'Segundo',
        nivel: 'Primaria'
      },
      {
        id_grado: 8,
        desc_grado: 'Tercero',
        nivel: 'Primaria'
      },
      {
        id_grado: 9,
        desc_grado: 'Cuarto',
        nivel: 'Primaria'
      },
      {
        id_grado: 10,
        desc_grado: 'Quinto',
        nivel: 'Primaria'
      },
      {
        id_grado: 11,
        desc_grado: 'Sexto',
        nivel: 'Primaria'
      },
      {
        id_grado: 12,
        desc_grado: 'Primero',
        nivel: 'Secundaria'
      },
      {
        id_grado: 13,
        desc_grado: 'Segundo',
        nivel: 'Secundaria'
      },
      {
        id_grado: 14,
        desc_grado: 'Tercero',
        nivel: 'Secundaria'
      },
      {
        id_grado: 15,
        desc_grado: 'Cuarto',
        nivel: 'Secundaria'
      },
      {
        id_grado: 16,
        desc_grado: 'Quinto',
        nivel: 'Secundaria'
      },
      {
        id_grado: 17,
        desc_grado: 'Cuna',
        nivel: '?'
      }
    ]

    grados.map((grado: CrearGradoDTO) => {
      this.gradoRepository.save(grado);
    })
  }
}
