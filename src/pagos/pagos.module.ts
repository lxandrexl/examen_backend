import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CronogramaEntity } from './entities/cronograma.entity';
import { DetalleCronogramaEntity } from './entities/detalle-cronograma.entity';
import { MovimientoEntity } from './entities/movimiento.entity';
import { PagosController } from './pagos.controller';
import { PagosService } from './pagos.service';

@Module({
  imports: [TypeOrmModule.forFeature([CronogramaEntity, DetalleCronogramaEntity, MovimientoEntity])],
  controllers: [PagosController],
  providers: [PagosService]
})
export class PagosModule {}
