import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity('cronograma')
export class CronogramaEntity {
    @PrimaryGeneratedColumn()
    id_cronograma: number;

    @Column({ type: "integer" })
    year: number;
}