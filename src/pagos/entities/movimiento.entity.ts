import { PersonaEntity } from "src/estudiantes/entities/persona.entity";
import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToOne } from "typeorm";
import { DetalleCronogramaEntity } from "./detalle-cronograma.entity";

@Entity('movimiento')
export class MovimientoEntity {
    @PrimaryGeneratedColumn()
    id_movimiento: number;

    @OneToOne(type => PersonaEntity)
    @JoinColumn({ name: 'id_persona'})
    id_persona: PersonaEntity;

    @OneToOne(type => DetalleCronogramaEntity)
    @JoinColumn({ name: 'id_detalle_cronograma'})
    id_detalle_cronograma: DetalleCronogramaEntity;

    @Column({ type: "varchar", length: 20 })
    tipo_movimiento: string;

    @Column({ type: "decimal", precision: 20, scale: 2, nullable: true})
    monto: number;

    @Column({ type: "varchar", length: 20 })
    estado: string;

    @Column({ type: "timestamp"})
    fecha_pago: Date;
   
}