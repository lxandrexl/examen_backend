import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from "typeorm";
import { CronogramaEntity } from "./cronograma.entity";

@Entity('detalle_cronograma')
export class DetalleCronogramaEntity {
    @PrimaryGeneratedColumn()
    id_detalle_cronograma: number;

    @OneToOne(type => CronogramaEntity)
    @JoinColumn({ name: 'id_cronograma'})
    id_cronograma: CronogramaEntity;

    @Column({ type: "varchar", length: 50 })
    desc_pension: string;

    @Column({ type: "decimal", precision: 20, scale: 2, nullable: true})
    monto: number;

    @Column({ type: "date"})
    fecha_venci: Date;
    
}