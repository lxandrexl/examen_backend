import { IsInt, IsNotEmpty, IsString } from "class-validator";

export class CrearGradoDTO {

    @IsInt()
    id_grado: number;

    @IsNotEmpty()
    @IsString()
    desc_grado: string;

    @IsNotEmpty()
    @IsString()
    nivel: string;
}