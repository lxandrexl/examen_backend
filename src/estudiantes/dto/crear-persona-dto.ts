import { IsEmail, IsNotEmpty, IsString, IsOptional, IsDate, IsNumber } from "class-validator";

export class CrearPersonaDTO {
    @IsNotEmpty()
    @IsString()
    nom_persona: string;

    @IsNotEmpty()
    @IsString()
    ape_pate_pers: string;

    @IsNotEmpty()
    @IsString()
    ape_mate_pers: string;

    @IsNotEmpty()
    @IsNumber()
    id_grado: number;

    @IsNotEmpty()
    @IsDate()
    fecha_naci: Date;

    @IsOptional()
    @IsString()
    foto_ruta: string;
}