import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity('grado')
export class GradoEntity {
    @PrimaryGeneratedColumn()
    id_grado?: number;

    @Column({ type: "varchar", length: 50 })
    desc_grado: string;
    
    @Column({ type: "varchar", length: 50 })
    nivel: string;
}