import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from "typeorm";
import { GradoEntity } from "./grados.entity";

@Entity('persona')
export class PersonaEntity {
    @PrimaryGeneratedColumn()
    id_persona?: number;

    @OneToOne(type => GradoEntity)
    @JoinColumn({ name: 'id_grado'})
    id_grado: number;

    @Column({ type: "varchar", length: 50 })
    nom_persona: string;

    @Column({ type: "varchar", length: 50 })
    ape_pate_pers: string;

    @Column({ type: "varchar", length: 50 })
    ape_mate_pers: string;

    @Column({ type: "date"})
    fecha_naci: Date;

    @Column({ type:"text",})
    foto_ruta: string;
    
}