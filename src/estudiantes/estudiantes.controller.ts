import { Body, Controller, Get, Post } from '@nestjs/common';
import { EstudiantesService } from './estudiantes.service';
import { CrearPersonaDTO } from "./dto/crear-persona-dto";
import { Persona } from './interfaces/persona.interface';
import { Grado } from './interfaces/grado.interface';
import { UploadFileHelp } from "./help/upload-file.help";

@Controller('estudiantes')
export class EstudiantesController {
    uploadHelp: UploadFileHelp = new UploadFileHelp();

    constructor(private estudiantesService: EstudiantesService) { }

    @Post()
    create(@Body() createPersonaDTO: CrearPersonaDTO): Promise<Persona> {
        createPersonaDTO.foto_ruta = this.uploadHelp.guardarImagen(createPersonaDTO.foto_ruta);
        return this.estudiantesService.crear(createPersonaDTO);
    }

    @Get()
    listar(): Promise<Persona[]> {
        return this.estudiantesService.listar();
    }

    @Get('grados')
    listarGrados(): Promise<Grado[]> {
        return this.estudiantesService.listarGrados();
    }

}
