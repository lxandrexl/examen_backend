import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { CrearPersonaDTO } from './dto/crear-persona-dto';
import { PersonaEntity } from './entities/persona.entity';
import { GradoEntity } from "./entities/grados.entity";
import { Persona } from "./interfaces/persona.interface";
import { Grado } from './interfaces/grado.interface';

@Injectable()
export class EstudiantesService {
    constructor(
        @InjectRepository(GradoEntity) private gradoRepository: Repository<GradoEntity>,
        @InjectRepository(PersonaEntity) private personaRepository: Repository<PersonaEntity>
    ) { }

    crear(crearPersonaDto: CrearPersonaDTO): Promise<Persona> {
        const user = this.personaRepository.create(crearPersonaDto);
        return this.personaRepository.save(user);
    }

    listar(): Promise<Persona[]> {
        return this.personaRepository.find();
        //return getRepository(PersonaEntity).createQueryBuilder("p").where("p.id_persona = :id", { id: 1 }).getMany();
    }

    listarGrados(): Promise<Grado[]> {
        return this.gradoRepository.find({ order: { id_grado: "ASC" } });
    }

}
