export interface Grado {
    readonly id_grado?: number;
    readonly desc_grado: string;
    readonly nivel: string;
}