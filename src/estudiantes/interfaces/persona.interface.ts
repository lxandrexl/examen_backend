export interface Persona {
    readonly id_persona?: number;
    readonly id_grado: number;
    readonly nom_persona: string;
    readonly ape_pate_pers: string;
    readonly ape_mate_pers: string;
    readonly fecha_naci: Date;
    readonly foto_ruta: string;
}
