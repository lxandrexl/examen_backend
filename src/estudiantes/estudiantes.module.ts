import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GradoEntity } from './entities/grados.entity';
import { PersonaEntity } from './entities/persona.entity';
import { EstudiantesController } from './estudiantes.controller';
import { EstudiantesService } from './estudiantes.service';

@Module({
  imports: [TypeOrmModule.forFeature([PersonaEntity, GradoEntity])],
  exports: [TypeOrmModule.forFeature([PersonaEntity, GradoEntity])],
  controllers: [EstudiantesController],
  providers: [EstudiantesService]
})
export class EstudiantesModule { }
