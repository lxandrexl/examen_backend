import * as moment from "moment";
import * as fs from "fs";
import { dirname } from "path";

export class UploadFileHelp {

    constructor() { }

    guardarImagen(base64): string {
        const fileName = `${moment().format('YYYYMMDDHHmmss')}.png`;
        const path = `./public/imgs/${fileName}`;
        let data = base64.replace(/^data:image\/png;base64,/, "");
        fs.promises
            .mkdir(dirname(path), {recursive: true})
            .then( () => fs.promises.writeFile(path, data, 'base64'));

        return fileName;
    }

}
