import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AudiMovimientoEntity } from './entities/audi-movimient.entity';
import { DocumentoNewEntity } from './entities/documento-new';
import { DocumentoXAudiMovimientoEntity } from './entities/documento-x-audi-movimiento';
import { ReportesController } from './reportes.controller';
import { ReportesService } from './reportes.service';

@Module({
  imports: [TypeOrmModule.forFeature([AudiMovimientoEntity, DocumentoNewEntity, DocumentoXAudiMovimientoEntity])],
  controllers: [ReportesController],
  providers: [ReportesService]
})
export class ReportesModule {}
