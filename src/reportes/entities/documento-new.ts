import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity('documento_new')
export class DocumentoNewEntity {
    @PrimaryGeneratedColumn()
    id_documento: number;

    @Column({ type: "varchar", length: 4 })
    nro_serie: string;

    @Column({ type: "varchar", length: 8 })
    nro_documento: string;

    @Column({ type: "varchar", length: 10 })
    tipo_documento: string;

    @Column({ type: "date"})
    fecha_emision: Date;
    
    @Column({ type: "jsonb"})
    jsonb_info: object;
}