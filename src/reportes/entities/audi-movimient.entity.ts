import { MovimientoEntity } from "src/pagos/entities/movimiento.entity";
import { Entity, Column, OneToOne, JoinColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity('audi_movimiento')
export class AudiMovimientoEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => MovimientoEntity)
    @JoinColumn({ name: 'id_movimiento' })
    id_movimiento: MovimientoEntity;

    @Column({ type: "integer" })
    correlativo: number;

    @Column({ type: "varchar", length: 15 })
    accion: string;

    @Column({ type: "decimal", precision: 20, scale: 2, nullable: true})
    monto_pagado: number;
}