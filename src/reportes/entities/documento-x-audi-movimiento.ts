import { Entity, Column, OneToOne, JoinColumn, PrimaryGeneratedColumn } from "typeorm";
import { DocumentoNewEntity } from "./documento-new";
import { MovimientoEntity } from "src/pagos/entities/movimiento.entity";

@Entity('documento_x_audi_movimiento')
export class DocumentoXAudiMovimientoEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => DocumentoNewEntity)
    @JoinColumn({ name: 'id_documento' })
    id_documento: DocumentoNewEntity;

    @OneToOne(type => MovimientoEntity)
    @JoinColumn({ name: 'id_movimiento' })
    id_movimiento: MovimientoEntity;

    @Column({ type: "integer" })
    correlativo: number;
}