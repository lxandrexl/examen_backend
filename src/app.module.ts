import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database.module';
import { EstudiantesModule } from './estudiantes/estudiantes.module';
import { PagosModule } from './pagos/pagos.module';
import { ReportesModule } from './reportes/reportes.module';

@Module({
  imports: [DatabaseModule, EstudiantesModule, PagosModule, ReportesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
